#include <stdio.h>
#include <stdlib.h>
#include "bitmap.h"


/*
 * Main filter loop.
 * This function is responsible for doing the following:
 *   1. Read in pixels one at a time (because copy is a pixel-by-pixel transformation).
 *   2. Immediately write out each pixel.
 *
 * Note that this function should allocate space only for a single Pixel;
 * do *not* store more than one Pixel at a time, it isn't necessary here!
 */
void greyscale_filter(Bitmap *bmp) {

  int i=0;
  int total_pixels = (bmp->height)*(bmp->width);

  while (i<total_pixels){
    Pixel * output_pixel = malloc(sizeof(Pixel));
    fread(output_pixel, sizeof(Pixel), 1, stdin);
    int avg = (output_pixel->red  + output_pixel->green + output_pixel->blue)/3;
    output_pixel->red = avg;
    output_pixel->green = avg;
    output_pixel->blue = avg;
    fwrite(output_pixel,sizeof(Pixel), 1,stdout);
    i++;
    free(output_pixel);

  }

}

int main() {
    // Run the filter program with copy_filter to process the pixels.
    // You shouldn't need to change this implementation.
    return 0;
}
