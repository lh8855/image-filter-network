

#include "request.h"
#include "response.h"
#include <string.h>


/******************************************************************************
 * ClientState-processing functions
 *****************************************************************************/
ClientState *init_clients(int n) {
    ClientState *clients = malloc(sizeof(ClientState) * n);
    for (int i = 0; i < n; i++) {
        clients[i].sock = -1;  // -1 here indicates available entry
    }
    return clients;
}

/*
 * Remove the client from the client array, free any memory allocated for
 * fields of the ClientState struct, and close the socket.
 */
void remove_client(ClientState *cs) {
    if (cs->reqData != NULL) {
        free(cs->reqData->method);
        free(cs->reqData->path);
        for (int i = 0; i < MAX_QUERY_PARAMS && cs->reqData->params[i].name != NULL; i++) {
            free(cs->reqData->params[i].name);
            free(cs->reqData->params[i].value);
        }
        free(cs->reqData);
        cs->reqData = NULL;
  } close(cs->sock);cs->sock = -1;cs->num_bytes = 0;
}


/*
 * Search the first inbuf characters of buf for a network newline ("\r\n").
 * Return the index *immediately after* the location of the '\n'
 * if the network newline is found, or -1 otherwise.
 * Definitely do not use strchr or any other string function in here. (Why not?)
 */
int find_network_newline(const char *buf, int inbuf) {
for (int i = 0; i < inbuf; i++){
      if (buf[i]=='\r' && buf[i+1]=='\n'){
        return i+2;
      }
    }
    return -1;
}

/*
 * Removes one line (terminated by \r\n) from the client's buffer.
 * Update client->num_bytes accordingly.
 *
 * For example, if `client->buf` contains the string "hello\r\ngoodbye\r\nblah",
 * after calling remove_line on it, buf should contain "goodbye\r\nblah"
 * Remember that the client buffer is *not* null-terminated automatically.
 */
void remove_buffered_line(ClientState *client) {
      int newline = find_network_newline(client->buf, client->num_bytes);
      if (newline!=-1){
         int move_this_much = client->num_bytes - (newline);
        memmove(&(client->buf[0]), &(client->buf[newline]),move_this_much);
        client->num_bytes -= (newline);
      }
}


/*
 * Read some data into the client buffer. Append new data to data already
 * in the buffer.  Update client->num_bytes accordingly.
 * Return the number of bytes read in, or -1 if the read failed.

 * Be very careful with memory here: there might be existing data in the buffer
 * that you don't want to overwrite, and you also don't want to go past
 * the end of the buffer, and you should ensure the string is null-terminated.
 */
int read_from_client(ClientState *client) {
    //IMPLEMENT THIS
    //this many bytes avaliable. read in as much as possible!

    //this is the first ever call. so num_bytes is 0.
    if (client->num_bytes==0){
      int  nbytes = read(client->sock, &(client->buf[0]), MAXLINE-1);
             if(nbytes <0){
               perror("read");
             }
             client->num_bytes += nbytes+1 ;
             //set null terminator.
             client->buf[client->num_bytes]='\0';
              printf("just read in %d\n",client->num_bytes );
       return client->num_bytes;
    }


    int left_to_read = MAXLINE - client->num_bytes -1;
    //read to the index at nm_bytes-1 so there's no overwriet.
    int  nbytes = read(client->sock, &(client->buf[client->num_bytes]), left_to_read);
           if(nbytes <0){
             perror("read");
           }
           client->num_bytes += nbytes+1;
           //set null terminator.
           client->buf[client->num_bytes]='\0';
            printf("just read in %d\n",client->num_bytes );
     return client->num_bytes;

}


/*****************************************************************************
 * Parsing the start line of an HTTP request.
 ****************************************************************************/
// Helper function declarations.
void parse_query(ReqData *req, const char *str);
void update_fdata(Fdata *f, const char *str);
void fdata_free(Fdata *f);
void log_request(const ReqData *req);


//helper function to determine parsing. returns type ot
//
int parse_query_helper(char *buffer){
  int path_index_end = 5;

  if (buffer[5]==' '){
    return 0; //no target
  }

  while(buffer[path_index_end]!= ' '){
    if (buffer[path_index_end]== '?'){
      return 1; //key pairs
    }
    path_index_end++;
  }

  return 2; //no key pairs
}




/* If there is a full line (terminated by a network newline (CRLF))
 * then use this line to initialize client->reqData
 * Return 0 if a full line has not been read, 1 otherwise.
 */
int parse_req_start_line(ClientState *client) {
    //first, find the terminateion index.
    int termination_index = find_network_newline(client->buf, client->num_bytes);
    if (termination_index == -1){
      return 0;
    }


    client->reqData = malloc(sizeof(ReqData));

    //case: get request
    if (client->buf[0] == GET[0]){
      client->reqData->method = malloc(sizeof(GET));
      client->reqData->method = GET ;
      int path_index_end = 4;
      int path_length = 0;
      int client_index = 0;

      //determine what kind of get request.
      int type = parse_query_helper(client->buf);

      //no paramter.
      if (type ==0){
        client->reqData->path = NULL;
      }

      //has question mark
      else if (type ==1){
      while(client->buf[path_index_end]!= '?'){
          path_index_end++;
          path_length = path_index_end-4;
      }
      char *client_buf = malloc(path_length+1);
      client->reqData->path = client_buf;
      strncpy(&client_buf[client_index], &(client->buf[4]), path_length);
      parse_query(client->reqData, &(client->buf[path_index_end+1]));

      }

      //parameters
      else if (type ==2){
        while(client->buf[path_index_end]!= ' '){
            path_index_end++;
            path_length = path_index_end-4;
        }
        char *client_buf = malloc(path_length+1);
        client->reqData->path = client_buf;
        strncpy(&client_buf[client_index], &(client->buf[4]), path_length);
      }

  }
  //post request.
  else if (client->buf[0] == POST[0]){
    client->reqData->method = malloc(sizeof(POST));

    client->reqData->method = POST;

    int path_index_end = 5;
    int path_length = 0;
    int client_index = 0;

    while(client->buf[path_index_end]!= ' '){
        path_index_end++;
        path_length = path_index_end-4;
    }
    printf("%d\n",path_length );
    char *client_buf = malloc(path_length+1);
    client->reqData->path = client_buf;
    strncpy(&client_buf[client_index], &(client->buf[5]), path_length-1);

    image_upload_response(client);

  }

  else {
    return 0;
  }
    // This part is just for debugging purposes.
    log_request(client->reqData);
    return 1;
}

/*
 * Initializes req->params from the key-value pairs contained in the given
 * string.
 * Assumes that the string is the part after the '?' in the HTTP request target,
 * e.g., name1=value1&name2=value2.
 */
void parse_query(ReqData *req, const char *str) {
    int current_index  = 0;
    int total_num_param = 0;
    int last_char_index = 0;
    int locations[11];

    //find out where the end of string is
    while (str[current_index]!= ' '){
      last_char_index ++;
      current_index++;
    }
    last_char_index++;
    current_index=0;

    //put into an array where the "indicator" indexs are.
    //will use these to determine what/how to parse.
    int i =0;
    while (current_index< last_char_index+1){
      if (str[current_index]=='=' ||str[current_index]=='&'|| str[current_index] ==' '){
        locations[i] = current_index;
        i++;
        if (str[current_index]=='='){
          total_num_param++;
        }
      }
      current_index++;
    }

    //working with the key-value pattern, parse the request!
    int beginning = 0;
    int middle = 0;
    int end = 0;
    int current_pair = 1;
    for (current_pair; current_pair< total_num_param+1; current_pair++){
      if (current_pair==1){
        beginning=0;
        middle=locations[0];
        end = locations[1];
       }
      else{
          beginning = end+1;
          middle = locations[(current_pair-1)*2];
          end = locations[(current_pair-1)*2+1];
      }
       Fdata * form_data = malloc(sizeof(Fdata));
       int length_of_name = middle - beginning-1;
       form_data->name = malloc(length_of_name+1);
       strncpy(form_data->name, &(str[beginning]),length_of_name+1);

       int length_of_value = end-middle-1;
       form_data->value = malloc(length_of_value+1);
       strncpy(form_data->value, &(str[middle+1]),length_of_value);
       req->params[current_pair-1] = * form_data;

    }

}




/*
 * Print information stored in the given request data to stderr.
 */
void log_request(const ReqData *req) {
    fprintf(stderr, "Request parsed: [%s] [%s]\n", req->method, req->path);
    for (int i = 0; i < MAX_QUERY_PARAMS && req->params[i].name != NULL; i++) {
        fprintf(stderr, "  %s -> %s\n",
                req->params[i].name, req->params[i].value);
    }
}


/***************************************** *************************************
 * Parsing multipart form data (image-upload)
 *****************************************************************************/


 char *get_boundary(ClientState *client) {
     int len_header = strlen(POST_BOUNDARY_HEADER);

     while (1) {
         int where = find_network_newline(client->buf, client->num_bytes);
         if (where > 0) {
             if (where < len_header || strncmp(POST_BOUNDARY_HEADER, client->buf, len_header) != 0) {
                 remove_buffered_line(client);
                 // printf("where2:%d\n",where);

             } else {
                printf("%s\n","found string!" );
                 // We've found the boundary string!
                 // We are going to add "--" to the beginning to make it easier
                 // to match the boundary line later
                 char *boundary = malloc(where - len_header + 1);
                 strncpy(boundary, "--", where - len_header + 1);
                 strncat(boundary, client->buf + len_header, where - len_header - 1);
                 boundary[where - len_header] = '\0';
                 return boundary;
             }
         } else {
             // Need to read more bytes
             if (read_from_client(client) <= 0) {
                 // Couldn't read; this is a bad request, so give up.
                 // printf("%s\n","read complete" );
                 return NULL;
             }
         }
     }
     // printf("%s\n","read complete" );

     return NULL;
 }

char *get_bitmap_filename(ClientState *client, const char *boundary) {
    int len_boundary = strlen(boundary);

    // Read until finding the boundary string.
    while (1) {
        int where = find_network_newline(client->buf, client->num_bytes);
        if (where > 0) {
            if (where < len_boundary + 2 ||
                    strncmp(boundary, client->buf, len_boundary) != 0) {
                remove_buffered_line(client);
            } else {
                // We've found the line with the boundary!
                 remove_buffered_line(client);
                break;
            }
        } else {
            // Need to read more bytes
            if (read_from_client(client) <= 0) {
                // Couldn't read; this is a bad request, so give up.
                return NULL;
            }
        }
    }
    printf("%s\n", "read_tillhere");

    int where = find_network_newline(client->buf, client->num_bytes);

    client->buf[where-1] = '\0';  // Used for strrchr to work on just the single line.
    char *raw_filename = strrchr(client->buf, '=') + 2;
    int len_filename = client->buf + where - 3 - raw_filename;
    char *filename = malloc(len_filename + 1);
    strncpy(filename, raw_filename, len_filename);
    filename[len_filename] = '\0';
    printf("%s\n",filename );

    // Restore client->buf
    client->buf[where - 1] = '\n';
    remove_buffered_line(client);
    return filename;
}

/*
 * Read the file data from the socket and write it to the file descriptor
 * file_fd.
 * You know when you have rea ched the end of the file in one of two ways:
 *    - search for the boundary string in each chunk of data read
 * (Remember the "\r\n" that comes before the boundary string, and the
 * "--\r\n" that comes after.)
 *    - extract the file size from the bitmap data, and use that to determine
 * how many bytes to read from the socket and write to the file
 */
int save_file_upload(ClientState *client, const char *boundary, int file_fd) {
    // Read in the next two lines: Content-Type line, and empty line
    remove_buffered_line(client);
    remove_buffered_line(client);
    int current_index=0;

    //get to offset 2, read in the file size. write the 2 bytes to the fd.
    int y = write(file_fd, &client->buf[current_index], 2);
    //update num_bytes
    client->num_bytes -= 2;

    int file_size=0;
    memcpy(&file_size, &client->buf[2], sizeof(int));

    //after the files size is read, read in the next 4 bytes into
    //the fd.
    write(file_fd, &client->buf[2],sizeof(int));
    client->num_bytes -= 4;

     //now, memove 6 bytes to index 0. want  a full buffer
    //for the upcomoing loop, so call read from client to fill up
    //the buffer.
    // memmove(&(client->buf[0]), &(client->buf[5]),client->num_bytes-7);
    // read_from_client(client); //NOW FILLED UP
    //read in everything inthe buffer right now.
    write(file_fd, &client->buf[5], client->num_bytes -= 4);



    //while left_to_read is greater than the space in the, just read on
    //MAXLINE-1 bytes until there aren't enough bytes left to read
    //to fill a full buffer.
    int left_to_read = file_size-6;
    int buffer[MAXLINE];
    while (left_to_read >= MAXLINE){
        left_to_read = left_to_read - read(client->sock, buffer,MAXLINE);
        write(file_fd,buffer, MAXLINE);
    };

    //since there are more bytes in the buffer than are required, just
    //write the amount there is left.
    if(left_to_read>0){
      left_to_read = left_to_read - read(client->sock, buffer,left_to_read);

      write(file_fd, buffer, left_to_read);
    }

    //
    // //while left_to_read is greater than the space in the, just read on
    // //MAXLINE-1 bytes until there aren't enough bytes left to read
    // //to fill a full buffer.
    // int left_to_read = file_size-6;
    // int buffer[MAXLINE];
    // while (left_to_read >= MAXLINE-1){
    //     write(file_fd, &client->buf[0], MAXLINE-1);
    //     left_to_read = left_to_read -(MAXLINE-1);
    //     //we can read in a whole new buffer now. so set numbytes to 1.
    //     client->num_bytes = 1;
    //     read_from_client(client);
    // };
    //
    // //since there are more bytes in the buffer than are required, just
    // //write the amount there is left.
    // if(left_to_read>0){
    //   write(file_fd, &client->buf[0], left_to_read);
    // }

return 0;
}

 
